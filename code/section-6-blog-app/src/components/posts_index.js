import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import PropTypes from 'prop-types'

import { fetchPosts } from '../actions/index'

class PostsIndex extends Component {
  static propTypes = {
    posts: PropTypes.array,
    fetchPosts: PropTypes.func
  }

  componentWillMount () {
    this.props.fetchPosts()
  }

  renderPost() {
    return this.props.posts.map((post) => {
      return (
        <Link to={`posts/${post.id}`} key={post.id}>
          <li className="list-group-item" >
            <span className="pull-xs-right">{post.categories}</span>
            <strong>{post.title}</strong>
          </li>
        </Link>
      )
    })
  }


  render () {
    return (
      <div>
        <div className='text-xs-right'>
          <Link to='posts/new' className='btn btn-primary'>
            Add a Post
          </Link>
        </div>
        <h3>Posts</h3>
        <ul className="list-group">
          {this.renderPost()}
        </ul>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return { posts: state.posts.all }
}

export default connect(mapStateToProps, { fetchPosts })(PostsIndex)
