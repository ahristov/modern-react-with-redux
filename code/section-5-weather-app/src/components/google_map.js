import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class GoogleMap extends Component {
  componentDidMount () {
    new google.maps.Map(this.map, {
      zoom: 12,
      center: {
        lat: this.props.lat,
        lng: this.props.lon
      }
    })
  }

  render () {
    // access with `this.refs.map`, it is deprecated
    // return <div ref='map' />

    // access with `this.map`
    return <div ref={c => { this.map = c }} />
  }
}

GoogleMap.propTypes = {
  lat: PropTypes.number,
  lon: PropTypes.number
}
