import React from 'react'
import PropTypes from 'prop-types'
import { Sparklines, SparklinesLine, SparklinesReferenceLine } from 'react-sparklines'
import _ from 'lodash'

function average (data) {
  return _.round(_.sum(data) / data.length)
}

// export default (props) => {
export default function Chart ({ data, color, units }) {
  return (
    <div>
      <Sparklines height={120} width={180} data={data}>
        <SparklinesLine color={color} />
        <SparklinesReferenceLine type='avg' />
      </Sparklines>
      <div>{average(data)} {units}</div>
    </div>
  )
}

Chart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.number),
  color: PropTypes.string,
  units: PropTypes.string
}
