import { FETCH_WEATHER } from '../actions'

// 'redux-promise':
//
// - sees the promise assigned into `payload`
// - stops the execution of the action until the promise is resolved
// - runs the request
// - dispatches a new action of the same type, but replaced the `payload` with the actual response from `axios`.
//
// We are receiving here on the `payload` unwrapped.
export default function (state = [], action) {
  switch (action.type) {
    case FETCH_WEATHER:
      return [action.payload.data, ...state]
  }

  return state
}
