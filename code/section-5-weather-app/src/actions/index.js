import axios from 'axios'

const API_KEY = 'cfc1328324216131d3488e53545de1fe'
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`

export const FETCH_WEATHER = 'FETCH_WEATHER'

export function fetchWeather (city) {
  const url = `${ROOT_URL}&q=${city},us`
  const request = axios.get(url)

  // We are returning the promise on the `payload`:
  return {
    type: FETCH_WEATHER,
    payload: request
  }
}
