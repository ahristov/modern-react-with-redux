import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchWeather } from '../actions'

class SearchBar extends Component {
  constructor (props) {
    super(props)

    this.state = { term: '' }

    this.onInputChange = this.onInputChange.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
  }

  onInputChange (event) {
    this.setState({ term: event.target.value })
  }

  onFormSubmit (event) {
    event.preventDefault()

    // We need to go and fetch weather data.
    this.props.fetchWeather(this.state.term)
    this.setState({ term: '' })
  }

  render () {
    return (
      <form
        className='input-group'
        onSubmit={this.onFormSubmit}>
        <input
          placeholder='Get a five-day forecast in your favorite cities'
          className='form-control'
          value={this.state.term}
          onChange={this.onInputChange}
        />
        <span className='input-group-btn'>
          <button
            type='submit'
            className='btn btn-secondary'>Submit</button>
        </span>
      </form>
    )
  }
}

SearchBar.propTypes = {
  fetchWeather: PropTypes.func
}

function mapDispatchToProps (dispatch) {
  // make sure the action creator flows into the middleware and Redux
  return bindActionCreators({ fetchWeather }, dispatch)
}

// argument 1: mapStateToProps - we don't need any state here
// argument 2: mapDispatchToProps
export default connect(null, mapDispatchToProps)(SearchBar)
