# Modern React with Redux

Contains code and notes from studying [Modern React with Redux](https://www.udemy.com/react-redux/learn/v4/overview)

## Intro

Get help:

- Email: ste.grider@gmail.com
- Twitter: @sg_in_sf
- Github: https://github.com/StephenGrider

You can find all of the code for this course here: [ReduxCasts](https://github.com/StephenGrider/ReduxCasts)

## Section 1: An intro to React

In this section we are using sample repository with started code from [ReduxSimpleStarter](https://github.com/StephenGrider/ReduxSimpleStarter)

Youtube API notes:

It need Google account. Go to console.developers.google.com, search for _Youtube data API v3_.
We need to _enable API_. Once it is done, from the _Credentials_ section get the API key. 
Click on _New Credentials_, then on _API key_.

Download and install package to search Youtube API:

    yarn add youtube-api-search

### Functional component

_Functional component_ is not aware of state, does not communicate to other components.

```js
import React from 'react'

const SearchBar = () => {
  return <input />
}

export default SearchBar
```

### Class component

_Class component_ is aware of state, ability to communicate with other components.

```js
import React, { Component } from 'react'

class SearchBar extends Component {
  render () {
    return <input />
  }
}

export default SearchBar
```

Using arrow functions:

```js
  render () {
    return <input onChange={event => console.log(event.target.value)} />
  }
```

### State

_State_ in class components:

Class components have state. It is a plain JS object that is used to record events. When the state is changed, the component will re-render.

REMEMBER: Functional components do not have state.

We need to initialize the state the object needs:

```js
  constructor (props) {
    super(props)

    this.state = { term: '' }
  }
```

Constructor is the only place we assign a state. Everywhere else we are using `this.setState()`.

### Controlled components

_Controlled components or Controlled field element_ is a form element who's value is set by the state rather than the other way around it.

Instead the input to tell what the state should be:

```js
<input onChange={event => this.setState({ term: event.target.value })} />
```

, the input field should get it's value from the state. We can add `value={this.state.term}` to make the field controlled.

If we remove completely the `onChange` event handler, we can't even type in the field:

```js
<input value={this.state.term} />
```

When we have both `onChange` and `value` parameters, what happens is: because the sets the state, it will re-render after that, the input element it is controlled by the state:

```js
<input
  value={this.state.term}
  onChange={event => this.setState({ term: event.target.value })}
```

## Section 2: Ajax Request with React

In this section we start fetching data fro the YouTube API. The question is, which component should fetch the data?

### Downwards data flow

In React there is the concept of _Downwards data flow_. It means, the most parent component in the application should be responsible for fetching data.

_Props_ arrive to functional component as function parameter like so:

```js
<VideoList videos={this.state.videos} />
...
const VideoList = (props) => { ...
```

When we are refactoring our component from functional for class based component, we need to also update all prop references from `props` to `this.props`.

### List item keys

Whenever we render a list, React expects to have a key. If key is not supplied, React will need to update the whole list. To make React render faster, we provide `key` parameter:

```js
const VideoList = (props) => {
  const videoItems = props.videos.map((video) => {
    return <VideoListItem key={video.etag} video={video} />
  })

  return (
    <ul className='col-md-4 list-group'>
      {videoItems}
    </ul>
  )
}
```

### ES6 syntax to name props

Instead of assigning props to variables:

```js
const VideoListItem = (props) => {
  const video = props.video
```

, we can use ES6 syntax, which does the same:

```js
const VideoListItem = ({vide}) => {
```

### Detail components and Template strings

Instead of concatenating strings:

```js
const url = 'https://www.youtube.com/embed/' + videoId
```

we can use string interpolation:

```js
const url = `https://www.youtube.com/embed/${videoId}`
```

### Handling Null Properties

When the app first renders, the collection `videos` is an empty array, thereof:

```js
<VideoDetail video={this.state.videos[0]} />
...
Uncaught TypeError: Cannot read property 'id' of undefined
```

throws an exception. So, we are passing undefined to the `VideoDetail`, and inside `VideoDetail` we need to check for `null`:

```js
const VideoDetail = ({ video }) => {
  if (!video) {
    return <div>Loading...</div>
  }
```

Note: Normally we do not want to spin whole lot of load spinner images all over the place, we usually do that on a top level component.

### CSS Class names

It is a good convention to have for every component, same top-level class name as the file name. For example `search_bar.js` would have top level `div className='search-bar'`.

### Searching for Videos

### Throttling the search

Using `lodash`, it's function `debounce`. What is does is, it makes sure a function can only be called once in certain period of time:

```js
const videoSearch = _.debounce((term) => { this.videoSearch(term) }, 300)
```

### Closing notes

THe class component `index.js` and `search_bar.js` both maintain state, but this is internal component state.

With _Redux_ we are maintaining application level state.

## Section 3: Modeling Application Stats

This section contains transition to Redux.

Every app has 2 parts:

- Data layer
- View Layer

For example a "Book store" app that has:

- List of books: positioned on the left side
- Details view: information for the selected book from the list on the right

![Book store layers](./doc/img/bookstore-app-UI-01.png)

has the following layers

- Data layer: List of books; Currently selected book
- View layer: List view; List item; Detail view

![Book store layers](./doc/img/bookstore-app-layers-01.png)

_Redux_ is the data layer of the app.

_React_ on the other hand represents the view.

The important concept of **Redux** is:

    It centralizes the application level state in one object.

note: One of the most important part of using Redux is - how we design the store. If we need to change often it will become a pain.

Example of complicated app - a "Tinder clone" app:

![Tinder app](./doc/img/tinder-app-UI-01.png)

The general flow is like this:

- The user is prompted with an image of another user

  - If the current user likes the other user, they will tap the "like" button

  - If they don't, they tap the "dislike" button

- Once the two users like each other, they become a "match"

- The matched users get a "chat" opened where they can have a "conversation" and send messages back and forth

- User can see list of all matches and open the chat with each of the matches

The data and the view parts would look something like:

![Tinder app](./doc/img/tinder-app-layers-01.png)

## Section 4: Managing App State with Redux

### Reducers

what is reducer? A **reducer** is:

    a function that returns a piece of the application state.

Because our application can have many different pieces of state, we can have multiple reducers.

In the "Book store" app, the application state has 2 pieces:

- List of books

- Currently selected book

We could have 2 different reducers:

![Tinder app](./doc/img/bookstore-app-reducers-01.png)

### Connecting redux to React

We create the reducer in `src/reducers/reducer_books.js`:

```js
export default () => {
  return [
    { title: 'Javascript: The Good Parts' },
    { title: 'Harry Potter' },
    { title: 'The Dark Tower' },
    { title: 'Eloquent Ruby' }
  ]
}
```

Then we create component to represent the list of books in `src/components/book_list.js`:

```js
import React, { Component } from 'react'
import PropTypes from 'prop-types'

class BookList extends Component {
  rederList () {
    return this.props.books.map((book) => {
      return (
        <li key={book.title} className='list-group-item'>{book.title}</li>
      )
    })
  }

  render () {
    return (
      <ul className='list-group col-sm-4'>
        {this.renderList()}
      </ul>
    )
  }
}

BookList.propTypes = {
  books: PropTypes.array
}

export default BookList
```

The question is now, how do we connect the component and the reducer?

How do we pass the array of books to the react component?

How do we plug-in the application state into the react component?

Connecting React and Redux is done by a separate library called [React-Redux](https://github.com/reactjs/react-redux)

What we have **to do** is:

    Promote one of the components as a Container.

A **container** is a React _component_ that has direct connection to the state managed by Redux.

In redux terminology sometimes the containers are called smart components in opposite to dumb components. The `src/components/book_list.js` is a dumb component.

To separate smart from dumb components we create different directories. We put the containers in a separate directory, so we move the books_list over to `src/containers/book_list.js`.

Question: How do we decide which component to promote as a container?

In general we want the most parent component that cares about the particular piece of state to be a container.

In our app this is the relation of the components to the data:

![Components relation to state](./doc/img/bookstore-app-component2container-01.png)

It makes sense both `BookList` and `BookDetail` to be promoted to containers as they care when the state has changed.

Following are the _steps_ we take to:

- transform the component (dumb component) into container (smart component)

- hook up the Redux state into the container

Have the initial code of the class component:

```js
import React, { Component } from 'react'
import PropTypes from 'prop-types'

class BookList extends Component {
  rederList () {
    return this.props.books.map((book) => {
      return (
        <li key={book.title} className='list-group-item'>{book.title}</li>
      )
    })
  }

  render () {
    return (
      <ul className='list-group col-sm-4'>
        {this.renderList()}
      </ul>
    )
  }
}

BookList.propTypes = {
  books: PropTypes.array
}

export default BookList
```

We:

- import `connect` from `react-redux`

- remove `export default`

- define `mapStatetToProps(state)` function

- use `connect` to turn the component into container

The new code:

```js
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class BookList extends Component {
  renderList () {
    return this.props.books.map((book) => {
      return (
        <li key={book.title} className='list-group-item'>{book.title}</li>
      )
    })
  }

  render () {
    return (
      <ul className='list-group col-sm-4'>
        {this.renderList()}
      </ul>
    )
  }
}

BookList.propTypes = {
  books: PropTypes.array
}

function mapStateToProps (state) {
  // Whatever is returned will show up as props
  // inside of BookList
  return {
    books: state.books
  }
}

export default connect(mapStateToProps)(BookList)
```

`connect` takes a function and a component and produces container. The container is a component that is aware of the state. `mapStateToProps` is the glue.

If the state ever changes:

- The object from the state will be re-assigned as props to the container

- The container will re-render

### Selecting particular book

We want to introduce the concept of an active book. This is the diagram of an **life-cycle of an action** selecting a book in e Redux application:

![Bookstore app actions](./doc/img/bookstore-app-actions-01.png)

The action life-cycle start with an **event**, either _direct_:

- clicking on a button

- selecting a book

, or _indirect_:

- page finished loading up

- ajax request finishing up

The events optionally call **action creator**. The action creator is a function that returns an **action object**.

That object then is automatically **sent to all reducers** inside our application.

Depending on the action, reducers can choose to **return a different piece of state**, depending on what the action is.

That newly returned state gets piped into the application state.

The application state then gets pumped into all the different containers, which causes the components to re-render.

### Building action creators

First we define an action inside `src/actions/index.js:

```js
export function selectBook (book) {
  console.log('A book has been selected: ', book.title)
}
```

Note: for now this is not an **ActionCreator** as it needs to return an object with property `type`.

How we gonna use it? We need to make sure the action that gets returned from it will be run by all reducers. We can't just call this function.

We need to **bind** the action to the components which will be using it.

In the `src/containers/book_list.js` we import the action `selectBook` and the `bindActionCreators` from Redux:

```js
import { selectBook } from '../actions/index'
import { bindActionCreators } from 'redux'
```

We add new function `mapDispatchToProps`

```js
// Anything returned from this function
// will end up as props on the BookList container
function mapDispatchToProps (dispatch) {
  // Whenever selectBook is called,
  // the result should be passed to all our reducers
  return bindActionCreators({
    selectBook: selectBook
  }, dispatch)
}
```

`bindActionCreators` with `dispatch` does:

    Takes the actions and passes down to all reducers.

Also:

    The returned object gets pushed to the `props`.

Finally at the bottom, we add `mapDispatchToProps` as another argument to `connect()`:

```js
export default connect(mapStateToProps, mapDispatchToProps)(BookList)
```

Now we can exercise the UI and watch for the console log. React actually will explain that the function `selectBook` is not a proper _ActionCreator_.

We need to return `type` and `payload`:

```js
export function selectBook (book) {
  // selectBook is an ActionCreator, it needs to return an action,
  // an object with a type property.
  // It returns a `type` and a payload.
  // `type`: Mandatory, capital letters, can be comma separated, will extract to constants later.
  // `payload`: Optinal, contains data.
  return {
    type: 'BOOK_SELECTED',
    payload: book
  }
}
```

Now we have an action that is piped out to all reducers.

### Consume the actions in reducers

All reducers get 2 arguments:

- `state`: is not application state, only the state this reducer is responsible for.

- `action`: an action object, we get `type` and `payload`

The reducer is not allowed to return `undefined`. We must always return a non undefined value:

```js
/**
 * State argument is not the application state, only the state
 * this reducer is responsible for.
 * Whatever the reducer generated, gets passed back to it again and again.
 * @param {any} [state=null] If undefined, set to null.
 * @param {any} action Action object
 * @returns new state
 */
export default function (state = null, action) {
  switch (action.type) {
    case 'BOOK_SELECTED':
      return action.payload // always return fresh object, never assign to state!
  }
  return state
}
```

We include in `reducers/index.js`:

```js
import { combineReducers } from 'redux'
import BooksReducer from './reducer_books'
import ActiveBookReducer from './reducer_active_book'

/*
const rootReducer = combineReducers({
  state: (state = {}) => state
})
*/

const rootReducer = combineReducers({
  books: BooksReducer,
  activeBook: ActiveBookReducer
})

export default rootReducer
```

REMEMBER: Any object with key provided to `combineReducers` ends up as a key on the global state!

Next we add `src/container/book_detail.js`, etc...

## Section 5: Intermediate Redux: Middleware

This chapter provides a `Weather forecast app`.

Challenges:

- We need to make AJAX requests from Redux
  - We used to make the AJAX requests from the events handlers inside the component
- We centralize as much as possible our logic inside reducers and actions
  - React components will only be responsible for showing data
  - They can tell Redux: _hey go fetch the data, that button was clicked_
- We will integrate 3rd party React graph library
- The state will change significantly
  - In the last section we only changed the active book

![Weather app](./doc/img/weather-app-UI-01.png)

We divide the app in different sections:

- SearchBar
  - Showing the search input and search button
  - Calling an action creator in Redux: tell Redux: _hey go search this data_
- ForecastList
  - General table component
  - We combine the List and ListRow components into one
- Chart
  - For each row we have 3 charts
- App
  - The overall glue application component

![Weather app](./doc/img/weather-app-UI-components-01.png)

### Controlled Components and Binding Context

Let's start with the search bar. Is it:

- a component
- or container?

It has the ability to dispatch actions: _someone enter a search term, we need to call an API to fetch data_.

Definitely we want to make it a container, because the containers will talk to Redux. Normal components only show data.

We add search bar: `src/containers/search_bar.js`.

We are going to turn the input field into a _controlled field_.
Controlled field is where the value of the input is set by the state of the component, not the other way around.

When someone clicks on the button, we are not going to say: _hey search by the value of the input bux_, rather than: _hey search by the value of the state this.state.etc..._:

```js
<input
  value={this.state.term}
  onChange={this.onInputChange}
```

Note: we are only referring to the component state, not to the global Redux state.

When we handle events on an element, how to deal with the problem with binding to `this`:

```js
onInputChange (event) {
  this.setState({ term: event.target.value })
}
...
<input
  onChange={this.onInputChange}>
```

we will get error message at runtime:

    Uncaught TypeError: Cannot read property 'setState' of undefined

**One option** is, use arrow function:

```js
onChange={event => this.onInputChange(event)}
```

**Second approach** is to bind the context of `onInoutChange` in the constructor:

```js
this.onInputChange = this.onInputChange.bind(this)
```

It says, take the existing function, replace it with new instance bound to the component.

Here is the complete example:

```js
import React, { Component } from 'react'

export default class SearchBar extends Component {
  constructor (props) {
    super(props)

    this.state = { term: '' }

    this.onInputChange = this.onInputChange.bind(this)
  }Weather forecast

  onInputChange (event) {
    this.setState({ term: event.target.value })
  }

  render () {
    return (
      <form className='input-group'>
        <input
          placeholder='Get a five-day forecast in your favorite cities'
          className='form-control'
          value={this.state.term}
          onChange={this.onInputChange}
        />
        <span className='input-group-btn'>
          <button type='submit' className='btn btn-secondary'>Submit</button>
        </span>
      </form>
    )
  }
}
```

It takes the existing function, binds to `this` and overrides it.

### Form elements

We are using a form to cut down the different paths to handle requestion data, so we have one handler. We also add `onSubmit` handler on the form to prevent default form submit to the server and thereof the page refresh.

### Weather data

We are going to use [OpenWeatherMap](http://openweathermap.com/forecast5) API.

The API key we store in `src/actions/index.js`

### Introduction to Middleware

Middleware is function that take an action, and depending on the action type or action payload, it may choose to:

- pass through
- manipulate the action
- console log it
- stop it altogether

This happens before it reaches any reducer.

![Middleware](./doc/img/redux-middleware-01.png)

We are going tp use `redux-promise`:

  yarn add redux-promise

To hook in the app, add to `src/index.js`:

```js
  import ReduxPromise from 'redux-promise'
  ...
  const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore)
```

We also install axios:

  yarn add axios

### redux-promise

Redux-promise is a great example of a middleware. 

We have the action creator `actions/index.js`, which returns a promise in the payload of the action:

```js
import axios from 'axios'

const API_KEY = 'cfc1328324216131d3488e53545de1fe'
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`

export const FETCH_WEATHER = 'FETCH_WEATHER'

export function fetchWeather (city) {
  const url = `${ROOT_URL}&q=${city},us`
  const request = axios.get(url)

  console.log('Request: ', request)

  // We are returning the promise on the `payload`:
  return {
    type: FETCH_WEATHER,
    payload: request
  }
}
```

But then we get in `reducers/reducer_weather.js` the actual response from the API server:

```js
// 'redux-promise':
//
// - sees the promise assigned into `payload`
// - stops the execution of the action until the promise is resolved
// - runs the request
// - dispatches a new action of the same type, but replaced the `payload` with the actual response from `axios`.
//
// We are receiving here on the `payload` unwrapped:
export default function (state = null, action) {
  console.log('Action received:', action)
  return state
}
```

![Redux-promise Example](./doc/img/redux-promise-middleware-01.png)

, where this is the flow-chart of the execution:

![Redux-promose flowchart](./doc/img/redux-promise-flowchart-01.png)

To create the list of cities, the Redux store will look like this:

![Redux store weather data](./doc/img/redux-store-weather-data-01.png)

See: `containers/weather_list.js`.

We need to install the npm package `react-sparklines@1.6.0`:

  yarn add react-sparklines@1.6.0

, and we'll also need `lodash`:

  yarn add lodash

and `react-google-maps`:

  yarn add react-google-maps

## Section 6: React Router and Redux Form

This chapter provides a `Simple Blogging application app`.

We are going to use remote API for the posts from [online source](reduxblog.herokuapp.com).

All the requests will provide simple `key` parameter unique per user, like:

  reduxblog.herokuapp.com/api/posts?key=123

We are installing `react-router`:

  yarn add react-router@2.0.0-rc5

This is what the React router does for us:

![React router](./doc/img/react-router-01.png)

### Wire up React Router

First in the index.js we want to replace the `<App/>` component from the `components/app.js`:

```js
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <App />
  </Provider>
  , document.querySelector('.container'));
```

, we won;t even need to reference the app component in this file anymore, so it becomes:

```js
import { Router, browserHistory } from 'react-router'
...

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={browserHistory} />
  </Provider>
  , document.querySelector('.container'));

```

History objects:

- `browserHistory`: whenever the URL changes, it informs the `Router`.
- `hashHistory`: operates on level hashes
- `memoryHistory`: does not use the url at all, useful for testing in native environments like React Native

### Route configuration

We create new file `routes.js`. Define the routes there:

```js
import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from './components/app'

export default (
  <Route path="/" component={App}/>
)
```

Import the routes in `index.js` and inject into `<Router>`:

```js
import routes from './routes'
...
<Router history={browserHistory} routers={routes} />
```

If we test the app navigating to:

  http://localhost:8080/

we will see the app, but if we enter:

  http://localhost:8080/posts

we get a warning in the console for url does not match a route.

### Nesting routes

Nesting routes is connected to nesting child components inside the `<App/>`.

If we, just for testing, have this functional component `<Greeting/>` and the defined bellow route nesting in `routes.js`:

```js
const Greeting = () => {
  return <div>Hi</div>
}

export default (
  <Route path="/" component={App}>
    <Route path="greet" component={Greeting} />
    <Route path="greet2" component={Greeting} />
    <Route path="greet3" component={Greeting} />
  </Route>
)
```

, so we have routes like '/', '/greet', '/greet3', etc.

We need to tell `<App/>` how to render the `<Greeting/>` as children, thereof we introduce `{this.props.children}` in `app.js`:

```js
import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        React simple starter
        {this.props.children}
      </div>
    );
  }
}
```

### IndexRoute

IndexRoute is a helper that behaves like a route. It will show whenever url matches up the parent.

Example:

```js
import PostsIndex from './components/posts_index'
...
export default (
  <Route path="/" component={App}>
    <IndexRoute component={PostsIndex} />
    ...
```

### Index actions

We need to install packages:

  yarn add axios redux-promise

, where:

- `axios`: use to create AJAX requests
- `redux-promise`: use to unwrap the AJAX responses

We also make sure we wire up the `redux-promise` as a middleware in `index.js`:

```js
import { createStore, applyMiddleware } from 'redux';
import promise from "redux-promise"
...
const createStoreWithMiddleware = applyMiddleware(
  promise
)(createStore)
```

Boiler plate for reducer:

```js
const INITIAL_STATE= {}

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    default:
      return state
  }
}
```

### Fetch data with component life cycle methods

When do we fetch the data? We do not have click event or user event. We need to do on life cycle method of the component.

We will be using `componentWillMount`.

Refactor the `posts_index.js` from functional component:

```js
import React from 'react'

export default () => {
  return <div>List of blog posts</div>
}
```

to class component:

```js
import React, { Component } from 'react'

class PostsIndex extends Component {
  render() {
    return (
      <div>List of blog posts</div>
    )
  }
}

export default PostsIndex
```

Then we add `copmponentWillMount` method on the `PostsIndex` class to run action `fetchPosts`:

```js
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators }  from 'redux'

import { fetchPosts } from '../actions/index'


class PostsIndex extends Component {
  componentWillMount() {
    this.props.fetchPosts()
  }

  render() {
    return (
      <div>List of blog posts</div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchPosts }, dispatch)
}

export default connect(null, mapDispatchToProps)(PostsIndex)
```

, which can have reduced the `mapDispatchToProps`:

```js
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { fetchPosts } from '../actions/index'

class PostsIndex extends Component {
  componentWillMount() {
    this.props.fetchPosts()
  }

  render() {
    return (
      <div>List of blog posts</div>
    )
  }
}

export default connect(null, { fetchPosts })(PostsIndex)
```

Next step is to map state to props, so the component will get the fetched data.

### Create new posts

On the list of posts page (`posts_index.js`) we add link to create post:

```js
import { Link } from 'react-router'
...
          <Link to='posts/new' className='btn btn-primary'>
            Add a Post
          </Link>
...
```

Implementing form to create new post with validation, which makes use of `redux-form` library:

  yarn add redux-form@4.1.3

First we need to hook up in the reducer `reducers/index.js`. We add it to the combined reducers, but it is important to:

- import with alias `formReducer` to avoid name conflicts
- include to the combinedReducers named as `form` (see: `posts_new.js`)

Example:

```js
import { reducer as formReducer } from 'redux-form'
...
const rootReducer = combineReducers({
  posts: PostsReducer,
  form: formReducer
})
```

This is the schema of using the `redux-form:

![Redux form](./doc/img/redux-form-01.png)

### Forms and form submission

We hook the `redux-form` similarly to `connect`:

```js
import React, { Component } from 'react'
import { reduxForm } from 'redux-form'

class PostsNew extends Component {
  render() {
    return (
      <div>Create new post</div>
    )
  }
}

export default reduxForm({
  form: 'PostsNewForm', // value has to be unique token, also - it is named 'form', here is the naming convention into place!
  fields: ['title', 'categories', 'content']
})(PostsNew)
```

Behind the scene, the `redux-form` will maintain state on the global redux store, something like:

```js
state === {
  form: {
    PostsNewForm: {
      title: '...',
      categories: '...',
      content: '...'
    }
  }
}
```

Also, the `redux-form` injects helpers `props` to the component:

- One helper is `handleSubmit()`
- It injects the `fields`

```js
  render () {
    const { fields: { title, categories, content }, handleSubmit } = this.props
    console.log(title)
...
      {name: "title", defaultChecked: false, defaultValue: undefined, initialValue: undefined, onBlur: ƒ, …}
      active:false
      defaultChecked:false
      defaultValue:undefined
      dirty:false
      initialValue:undefined
      invalid:false
      name:"title"
      onBlur:ƒ (event)
      onChange:ƒ (event)
      onDragStart:ƒ (event)
      onDrop:ƒ (event)
      onFocus:ƒ ()
      onUpdate:ƒ (event)
      pristine:true
      touched:false
      valid:true
      visited:false
```

, then we can destructure all of the props, for the element _title_:

```js
  <input type='text' className='form-control' {...title} />
```

Also, `reduxForm()` wraps the component in the exact same way as the `connect()`, only that is has one extra parameter - the configuration with the form and fields:

- `connect`: 1st argument is `mapStateToProps`, 2nd is `mapDispatchToProps`
- `reduxForm`: 1st argument is `form config`, 2nd argument is `mapStateToProps`, 3rd is `mapDispatchToProps`

### Validation

Create validation function passed to `reduxForm()` as part of the `form config` object:

```js
function validate (values) {
  const errors = {}

  if (!values.title) {
    errors.title = 'Enter a username'
  }

  return errors
}

export default reduxForm({
  form: 'PostsNewForm', // value has to be unique token, also - it is named 'form', here is the naming convention into place!
  fields: ['title', 'categories', 'content'],
  validate
}, null, { createPost })(PostsNew)
```

Then we can present the validation errors on the form:

```js
      <form onSubmit={handleSubmit(createPost)}>
        <h3>Create A New Post</h3>

        <div className={`form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
          <label>Title</label>
          <input type='text' className='form-control' {...title} />
          <div className='text-help'>
            {title.touched ? title.error : ''}
          </div>
        </div>
```

, where `touched` fires on element blur, form submit etc.

### Navigating on Submit

We are using from `react-router`, it's method `push()`.

We get access to the the property `context`, which gets passed from parent to child component in React.
The difference between `props` and `context` is, the `context` does not get explicitly passed to the child components like we have to do with the `props`.
It is implicitly passed.

IMPORTANT: We should avoid using the `context` for our apps as much as possible. The only time we should be using the `context` is, specifically when working with `react-router`.

When we are declaring access to `router` from the `context`, React will traverse up on the parents until it finds it"

```js
class PostsNew extends Component {
  static propTypes = {
    fields: PropTypes.object,
    handleSubmit: PropTypes.func,
    createPost: PropTypes.func
  }

  static contextTypes = {
    router: PropTypes.object
  }
```



